FROM node:12.13.0

# Bundle app source
COPY src /src/
COPY package.json collect.js config.js /

RUN cd /; npm install

CMD ["node", "collect.js"]