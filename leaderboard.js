'use strict';

require('dotenv').config()
const config = require('./config');
const PlayFabService = require('./src/PlayFabService');

(async () => {
    try {
        const playFabSettings = {
            titleId: config.PLAYFAB_TITLE_ID,
            secretKey: config.PLAYFAB_SECRET_KEY,
        };

        const playFab = new PlayFabService(playFabSettings);
        const leaderboard = await playFab.getFullLeaderboard();
        console.log(`Leaderboard count: ${leaderboard.length}`);


        process.exit(0);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
})();
