'use strict';

module.exports = {
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    AWS_REGION: process.env.AWS_REGION,
    DYNAMODB_TABLE_NAME: process.env.DYNAMODB_TABLE_NAME,
    DYNAMODB_READ_THROUGHPUT: process.env.DYNAMODB_READ_THROUGHPUT,
    PLAYFAB_TITLE_ID: process.env.PLAYFAB_TITLE_ID,
    PLAYFAB_SECRET_KEY: process.env.PLAYFAB_SECRET_KEY,
    AYR_SHARE_KEY: process.env.AYR_SHARE_KEY,
};