{
  "code": 200,
  "status": "OK",
  "data": {
    "Leaderboard": [
      {
        "PlayFabId": "80067D5694C0B8F4",
        "DisplayName": "Innocent Samurai 0042",
        "StatValue": 5723,
        "Position": 0,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "80067D5694C0B8F4",
          "DisplayName": "Innocent Samurai 0042"
        }
      },
      {
        "PlayFabId": "8ED45F7D2D20F523",
        "DisplayName": "Happy Mouse 3866",
        "StatValue": 4014,
        "Position": 1,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "8ED45F7D2D20F523",
          "DisplayName": "Happy Mouse 3866"
        }
      },
      {
        "PlayFabId": "726B7CD3C5F66DDD",
        "DisplayName": "Open Sourcerer A",
        "StatValue": 3622,
        "Position": 2,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "726B7CD3C5F66DDD",
          "DisplayName": "Open Sourcerer A"
        }
      },
      {
        "PlayFabId": "C963B81E9B3D1D22",
        "DisplayName": "Denis the Menace",
        "StatValue": 3312,
        "Position": 3,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "C963B81E9B3D1D22",
          "DisplayName": "Denis the Menace"
        }
      },
      {
        "PlayFabId": "F3A52385F1751286",
        "DisplayName": "Tense Engineer 4716",
        "StatValue": 3053,
        "Position": 4,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "F3A52385F1751286",
          "DisplayName": "Tense Engineer 4716"
        }
      },
      {
        "PlayFabId": "A88E1FA44567BA6D",
        "DisplayName": "Prisoner",
        "StatValue": 1730,
        "Position": 5,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "A88E1FA44567BA6D",
          "DisplayName": "Prisoner"
        }
      },
      {
        "PlayFabId": "71D7528629E3FED4",
        "DisplayName": "Weary Barracuda 8388",
        "StatValue": 1559,
        "Position": 6,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "71D7528629E3FED4",
          "DisplayName": "Weary Barracuda 8388"
        }
      },
      {
        "PlayFabId": "9BA77F3E2193F37D",
        "DisplayName": "Fair Toad 1935",
        "StatValue": 1330,
        "Position": 7,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "9BA77F3E2193F37D",
          "DisplayName": "Fair Toad 1935"
        }
      },
      {
        "PlayFabId": "AD6E7671681F6F1E",
        "DisplayName": "Open Sourcerer G",
        "StatValue": 1211,
        "Position": 8,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "AD6E7671681F6F1E",
          "DisplayName": "Open Sourcerer G"
        }
      },
      {
        "PlayFabId": "4731224E83B043B7",
        "DisplayName": "Graceful Phoenix 5458",
        "StatValue": 1088,
        "Position": 9,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "4731224E83B043B7",
          "DisplayName": "Graceful Phoenix 5458"
        }
      },
      {
        "PlayFabId": "5F90972547670A6D",
        "DisplayName": "Happy Snail 1985",
        "StatValue": 963,
        "Position": 10,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "5F90972547670A6D",
          "DisplayName": "Happy Snail 1985"
        }
      },
      {
        "PlayFabId": "525AFC15DE0018C6",
        "DisplayName": "Long Rock Star 2394",
        "StatValue": 651,
        "Position": 11,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "525AFC15DE0018C6",
          "DisplayName": "Long Rock Star 2394"
        }
      },
      {
        "PlayFabId": "77128D8ABEEF33A8",
        "DisplayName": "Breakable Husky 8876",
        "StatValue": 245,
        "Position": 12,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "77128D8ABEEF33A8",
          "DisplayName": "Breakable Husky 8876"
        }
      },
      {
        "PlayFabId": "7BF2B70519987263",
        "DisplayName": "Wide-eyed Sheepdog 1220",
        "StatValue": 204,
        "Position": 13,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "7BF2B70519987263",
          "DisplayName": "Wide-eyed Sheepdog 1220"
        }
      },
      {
        "PlayFabId": "F8B2CA4D683ABB69",
        "DisplayName": "Powerful Termite 0450",
        "StatValue": 189,
        "Position": 14,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "F8B2CA4D683ABB69",
          "DisplayName": "Powerful Termite 0450"
        }
      },
      {
        "PlayFabId": "39A2FC55CB092565",
        "DisplayName": "Lonely President 4366",
        "StatValue": 156,
        "Position": 15,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "39A2FC55CB092565",
          "DisplayName": "Lonely President 4366"
        }
      },
      {
        "PlayFabId": "C139D96647795702",
        "DisplayName": "Faithful Hyena 5041",
        "StatValue": 130,
        "Position": 16,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "C139D96647795702",
          "DisplayName": "Faithful Hyena 5041"
        }
      },
      {
        "PlayFabId": "1C258039C67E33C8",
        "DisplayName": "Scary Manatee 6103",
        "StatValue": 128,
        "Position": 17,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "1C258039C67E33C8",
          "DisplayName": "Scary Manatee 6103"
        }
      },
      {
        "PlayFabId": "A8FA1660DB40AB6C",
        "DisplayName": "Faithful Gnome 2439",
        "StatValue": 126,
        "Position": 18,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "A8FA1660DB40AB6C",
          "DisplayName": "Faithful Gnome 2439"
        }
      },
      {
        "PlayFabId": "36D38DBA5A3A268D",
        "DisplayName": "Real Basilisk 9057",
        "StatValue": 123,
        "Position": 19,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "36D38DBA5A3A268D",
          "DisplayName": "Real Basilisk 9057"
        }
      },
      {
        "PlayFabId": "3BE7B4345C34C7E8",
        "DisplayName": "Energetic Goblin 6118",
        "StatValue": 101,
        "Position": 20,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "3BE7B4345C34C7E8",
          "DisplayName": "Energetic Goblin 6118"
        }
      },
      {
        "PlayFabId": "C75EB344678C95D",
        "DisplayName": "Perfect Ninja 1320",
        "StatValue": 71,
        "Position": 21,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "C75EB344678C95D",
          "DisplayName": "Perfect Ninja 1320"
        }
      },
      {
        "PlayFabId": "BBBA0FEA61BCDFA3",
        "DisplayName": "Crowded Carpenter 5482",
        "StatValue": 37,
        "Position": 22,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "BBBA0FEA61BCDFA3",
          "DisplayName": "Crowded Carpenter 5482"
        }
      },
      {
        "PlayFabId": "6A1FC458A999376C",
        "DisplayName": "Calm Jellyfish 4953",
        "StatValue": 20,
        "Position": 23,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "6A1FC458A999376C",
          "DisplayName": "Calm Jellyfish 4953"
        }
      },
      {
        "PlayFabId": "55B95A4BA9889EE7",
        "DisplayName": "Tender Chimera 2963",
        "StatValue": 20,
        "Position": 24,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "55B95A4BA9889EE7",
          "DisplayName": "Tender Chimera 2963"
        }
      },
      {
        "PlayFabId": "180C5CE7D1606F81",
        "DisplayName": "Alert Ocelot 7220",
        "StatValue": 20,
        "Position": 25,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "180C5CE7D1606F81",
          "DisplayName": "Alert Ocelot 7220"
        }
      },
      {
        "PlayFabId": "CCCBAA8B418DB59B",
        "DisplayName": "Alive Ostrich 4896",
        "StatValue": 20,
        "Position": 26,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "CCCBAA8B418DB59B",
          "DisplayName": "Alive Ostrich 4896"
        }
      },
      {
        "PlayFabId": "A813E4CBC56AC7E9",
        "DisplayName": "Clumsy Goblin 1915",
        "StatValue": 20,
        "Position": 27,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "A813E4CBC56AC7E9",
          "DisplayName": "Clumsy Goblin 1915"
        }
      },
      {
        "PlayFabId": "5A0C0D8A95F6B25F",
        "DisplayName": "Mushy Chipmunk 3291",
        "StatValue": 0,
        "Position": 28,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "5A0C0D8A95F6B25F",
          "DisplayName": "Mushy Chipmunk 3291"
        }
      },
      {
        "PlayFabId": "43FA5C9A7B0F4FA3",
        "DisplayName": "Gleaming Planner 4454",
        "StatValue": 0,
        "Position": 29,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "43FA5C9A7B0F4FA3",
          "DisplayName": "Gleaming Planner 4454"
        }
      },
      {
        "PlayFabId": "43C9E42C2FC97F60",
        "DisplayName": "Fancy Octopus 3605",
        "StatValue": 0,
        "Position": 30,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "43C9E42C2FC97F60",
          "DisplayName": "Fancy Octopus 3605"
        }
      },
      {
        "PlayFabId": "354E230345F6D658",
        "DisplayName": "Crowded Skunk 5815",
        "StatValue": 0,
        "Position": 31,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "354E230345F6D658",
          "DisplayName": "Crowded Skunk 5815"
        }
      },
      {
        "PlayFabId": "32831EC80CC978EF",
        "DisplayName": "Confused Architect 2480",
        "StatValue": 0,
        "Position": 32,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "32831EC80CC978EF",
          "DisplayName": "Confused Architect 2480"
        }
      },
      {
        "PlayFabId": "2520F6F36C3679A6",
        "DisplayName": "Relieved Hero 0463",
        "StatValue": 0,
        "Position": 33,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "2520F6F36C3679A6",
          "DisplayName": "Relieved Hero 0463"
        }
      },
      {
        "PlayFabId": "F5265183B120F73B",
        "DisplayName": "Brave Zombie 6568",
        "StatValue": 0,
        "Position": 34,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "F5265183B120F73B",
          "DisplayName": "Brave Zombie 6568"
        }
      },
      {
        "PlayFabId": "DE42196777A5B9E8",
        "DisplayName": "Average Warthog 1176",
        "StatValue": 0,
        "Position": 35,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "DE42196777A5B9E8",
          "DisplayName": "Average Warthog 1176"
        }
      },
      {
        "PlayFabId": "A366F4A32BC32B03",
        "DisplayName": "Fancy Dragon 3660",
        "StatValue": 0,
        "Position": 36,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "A366F4A32BC32B03",
          "DisplayName": "Fancy Dragon 3660"
        }
      },
      {
        "PlayFabId": "87C18761EBF34A04",
        "DisplayName": "Jittery Wolf 3810",
        "StatValue": 0,
        "Position": 37,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "87C18761EBF34A04",
          "DisplayName": "Jittery Wolf 3810"
        }
      },
      {
        "PlayFabId": "1A02E34B890FC4E5",
        "DisplayName": "Perfect Tiger 3046",
        "StatValue": -2,
        "Position": 38,
        "Profile": {
          "PublisherId": "F54412DE14142BE7",
          "TitleId": "A7860",
          "PlayerId": "1A02E34B890FC4E5",
          "DisplayName": "Perfect Tiger 3046"
        }
      }
    ],
    "Version": 8,
    "NextReset": "2021-01-11T00:00:00Z"
  }
}
{
  "code": 200,
  "status": "OK",
  "data": {
    "Leaderboard": [],
    "Version": 8,
    "NextReset": "2021-01-11T00:00:00Z"
  }
}
Leaderboard count: 39
