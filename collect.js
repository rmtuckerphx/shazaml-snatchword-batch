'use strict';
const moment = require('moment');
require('dotenv').config()
const config = require('./config');

module.exports.handler = async event => {
    console.log('handler - start');

    try {
        let sourceConnectionOptions = {
            region: config.AWS_REGION,
            accessKeyId: config.AWS_ACCESS_KEY_ID,
            secretAccessKey: config.AWS_SECRET_ACCESS_KEY
        };

        const playFabSettings = {
            titleId: config.PLAYFAB_TITLE_ID,
            secretKey: config.PLAYFAB_SECRET_KEY,
        };

        const ayrSettings = {
            secretKey: config.AYR_SHARE_KEY,
        };

        const metadata = {
            filterExpression: '#t = :t AND #x <= :d AND #o <> :o',
            expressionAttributeNames: {
                '#t': 'type',
                '#x': 'expiresAt',
                '#o': 'owner',
            },
            expressionAttributeValues: {
                ':t': 'WORD',
                ':d': moment().subtract(4, 'd').toISOString(),
                ':o': 'SYSTEM::0'
            },
        }

        const CollectionJob = require('./src/CollectionJob');
        const job = new CollectionJob(config.DYNAMODB_TABLE_NAME, sourceConnectionOptions, 100, config.DYNAMODB_READ_THROUGHPUT, playFabSettings, ayrSettings);
        job.setSourcefilterExpression(metadata.filterExpression, metadata.expressionAttributeNames, metadata.expressionAttributeValues);

        console.log('Running update...')
        await job.run();
        // process.exit(0);
    } catch (error) {
        console.error('CollectionJob error', error);
        // process.exit(1);
    }

    return event;
};

// (async () => {
//     try {
//         let sourceConnectionOptions = {
//             region: config.AWS_REGION,
//             accessKeyId: config.AWS_ACCESS_KEY_ID,
//             secretAccessKey: config.AWS_SECRET_ACCESS_KEY
//         };

//         const playFabSettings = {
//             titleId: config.PLAYFAB_TITLE_ID,
//             secretKey: config.PLAYFAB_SECRET_KEY,
//         };

//         const ayrSettings = {
//             secretKey: config.AYR_SHARE_KEY,
//         };

//         const metadata = {
//             filterExpression: '#t = :t AND #x <= :d AND #o <> :o',
//             expressionAttributeNames: {
//                 '#t': 'type',
//                 '#x': 'expiresAt',
//                 '#o': 'owner',
//             },
//             expressionAttributeValues: {
//                 ':t': 'WORD',
//                 ':d': moment().subtract(4, 'd').toISOString(),
//                 ':o': 'SYSTEM::0'
//             },
//         }

//         const CollectionJob = require('./src/CollectionJob');
//         const job = new CollectionJob(config.DYNAMODB_TABLE_NAME, sourceConnectionOptions, 100, config.DYNAMODB_READ_THROUGHPUT, playFabSettings, ayrSettings);
//         job.setSourcefilterExpression(metadata.filterExpression, metadata.expressionAttributeNames, metadata.expressionAttributeValues);

//         console.log('Running update...')
//         await job.run();
//         process.exit(0);
//     } catch (error) {
//         console.error('CollectionJob error', error);
//         process.exit(1);
//     }
// })();