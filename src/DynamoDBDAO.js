'use strict';
const moment = require('moment');
const DynamoDBConnector = require('./DynamoDBConnector');

class DynamoDBDAO {
    constructor(tableName, region, accessKeyId, secretAccessKey) {
        this.tableName = tableName;
        this.docClient = DynamoDBConnector.getDocumentClient(region);
        // this.docClient = DynamoDBConnector.getDocumentClient(region, accessKeyId, secretAccessKey);
    }

    query(keyConditionExpression, filterExp, expAttrNames, expAttrValues, indexName, projExp) {
        let ctx = this;
        return new Promise((resolve, reject) => {
            try {
                let params = {
                    TableName: this.tableName
                };
                if (keyConditionExpression) {
                    params.KeyConditionExpression = keyConditionExpression;
                }
                if (indexName) {
                    params.IndexName = indexName;
                }
                if (filterExp) {
                    params.FilterExpression = filterExp;
                }
                if (expAttrNames) {
                    params.ExpressionAttributeNames = expAttrNames;
                }
                if (expAttrValues) {
                    params.ExpressionAttributeValues = expAttrValues;
                }
                if (projExp) {
                    params.ProjectionExpression = projExp;
                }
                ctx.docClient.query(params, function (error, response) {
                    if (error) {
                        console.error(error);
                        reject(error);
                    } else {
                        resolve(response);
                    }
                });
            } catch (error) {
                console.error(error);
                reject(reject);
            }
        });
    }

    scan(filterExp, expAttrNames, expAttrValues, lastEvalKey, limit) {
        let ctx = this;
        return new Promise((resolve, reject) => {
            try {
                let params = {
                    TableName: this.tableName
                };
                if (filterExp) {
                    params.FilterExpression = filterExp;
                }
                if (expAttrNames) {
                    params.ExpressionAttributeNames = expAttrNames;
                }
                if (expAttrValues) {
                    params.ExpressionAttributeValues = expAttrValues;
                }
                if (limit) {
                    params.Limit = limit;
                }
                if (lastEvalKey) {
                    params.ExclusiveStartKey = lastEvalKey;
                }
                params.ReturnConsumedCapacity = 'TOTAL';
                ctx.docClient.scan(params, function (error, response) {
                    if (error) {
                        console.error(error);
                        reject(error);
                    } else {
                        resolve(response);
                    }
                });
            } catch (error) {
                console.error(error);
                reject(reject);
            }
        });
    }

    async getUser(userId) {
        // console.trace(`DynamoDBDAO.getUser - userId: ${userId}`);

        const params = {
            TableName: this.tableName,
            Key: {
                PK: `USER#${userId}`,
                SK: `USER#${userId}`,
            },
        };

        console.log('params:', JSON.stringify(params, null, 2));

        try {
            const data = await this.docClient.get(params).promise();

            if (Object.keys(data).length === 0 && data.constructor === Object) {
                return {
                    status: 'NOT_FOUND',
                };
            }

            const totalPoints = data.Item.bonusPoints + data.Item.points;

            return {
                status: 'SUCCESS',
                data: data.Item,
                totalPoints,
            };
        } catch (error) {
            console.error(error);
            return {
                status: error.code,
                error,
            };
        }

    }

    async collectByLibrarian(word,
        oldOwner,
        totalPoints,
        newUserId,
        newPlayerName) {
        // eslint-disable-next-line max-len
        // log.trace(`databaseHelper.snatchOwnedByOther - word: ${word}, oldOwner: ${oldOwner}, totalPoints: ${totalPoints}, newUserId: ${newUserId}, newPlayerName: ${newPlayerName}`);

        // const now = new Date().toISOString();
        // const max = new Date(253402214400000).toISOString();

        // const snatchAction = ignoreExpiresAt ? 'STOLEN' : 'SNATCHED';
        // const snatchAction = 'COLLECTED';

        const params = {
            TransactItems: [
                {
                    Update: {
                        TableName: this.tableName,
                        Key: {
                            PK: `WORD#${word.toUpperCase()}`,
                            SK: `WORD#${word.toUpperCase()}`,
                        },
                        UpdateExpression: 'SET #o = :u, #x = :d, #gpk = :k, #p = :p',
                        ConditionExpression: 'attribute_exists(PK) AND #o <> :u',
                        ExpressionAttributeNames: {
                            '#x': 'expiresAt',
                            '#o': 'owner',
                            '#gpk': 'PK1',
                            '#p': 'player',
                        },
                        ExpressionAttributeValues: {
                            ':d': moment().add(4, 'd').toISOString(),
                            ':u': newUserId,
                            ':k': `USER#${newUserId}`,
                            ':p': newPlayerName,
                        },
                        ReturnValues: 'ALL_OLD',
                    },
                },
                {
                    Update: {
                        TableName: this.tableName,
                        Key: {
                            PK: `USER#${newUserId}`,
                            SK: `USER#${newUserId}`,
                        },
                        UpdateExpression: 'ADD #p :p',
                        ConditionExpression: 'attribute_exists(PK)',
                        ExpressionAttributeNames: {
                            '#p': 'points',
                        },
                        ExpressionAttributeValues: {
                            ':p': totalPoints,
                        },
                        ReturnValues: 'ALL_OLD',
                    },
                },
                {
                    Update: {
                        TableName: this.tableName,
                        Key: {
                            PK: `USER#${oldOwner}`,
                            SK: `USER#${oldOwner}`,
                        },
                        UpdateExpression: 'ADD #p :p',
                        ConditionExpression: 'attribute_exists(PK)',
                        ExpressionAttributeNames: {
                            '#p': 'points',
                        },
                        ExpressionAttributeValues: {
                            ':p': totalPoints * -1,
                        },
                        ReturnValues: 'ALL_OLD',
                    },
                },
                // {
                //   Put: {
                //     TableName: process.env.DYNAMODB_USERS_TABLE,
                //     Item: {
                //       PK: `USER#${oldOwner}`,
                //       SK: `SNATCH#${word.toUpperCase()}`,
                //       type: 'SNATCH',
                //       action: snatchAction,
                //       word,
                //       createAt: now,
                //       newPlayerName,
                //       ttl: Math.floor(Date.now() / 1000) + 86400, //24 hours in future
                //     },
                //     ReturnValues: 'ALL_OLD',
                //     ConditionExpression: 'attribute_not_exists(PK)',
                //   },
                // }
            ],
        };

        console.log('params:', JSON.stringify(params, null, 2));

        try {
            const data = await this.executeTransactWrite(params);

            return {
                status: 'SUCCESS',
                data,
                // action: snatchAction,
            };
        } catch (error) {
            console.error(error);
            return {
                status: error.code,
                error,
            };
        }

    }

    executeTransactWrite(params) {
        const transactionRequest = this.docClient.transactWrite(params);
        let cancellationReasons;
        transactionRequest.on('extractError', (response) => {
            try {
                cancellationReasons = JSON.parse(response.httpResponse.body.toString()).CancellationReasons;
            } catch (err) {
                // suppress this just in case some types of errors aren't JSON parseable
                console.error('Error extracting cancellation error', err);
            }
        });
        return new Promise((resolve, reject) => {
            transactionRequest.send((err, response) => {
                if (err) {
                    console.error('Error performing transactWrite', { cancellationReasons, err });
                    return reject(err);
                }
                return resolve(response);
            });
        });
    }
}

module.exports = DynamoDBDAO;

