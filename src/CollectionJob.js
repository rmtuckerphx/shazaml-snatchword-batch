'use strict';
const RateLimiter = require('limiter').RateLimiter;
const DynamoDBDAO = require('./DynamoDBDAO');
const PlayFabService = require('./PlayFabService');
// const _findIndex = require('lodash.findindex');
const PostSocialService = require('./PostSocialService');
const { ProfanityValidator } = require('jovo-community-validators');
const profanityValidatorConfig = require('./profanityValidatorConfig.json');

class CollectionJob {
    constructor(tableName, sourceConnectionOptions, dynamodbEvalLimit, dynamoDbReadThroughput, playFabSettings, ayrSettings) {
        this.tableName = tableName;
        // this.filterFunction = () => { return true; };
        this.dynamoDBDAO = new DynamoDBDAO(tableName, sourceConnectionOptions.region, sourceConnectionOptions.accessKeyId, sourceConnectionOptions.secretAccessKey);
        this.dynamodbEvalLimit = dynamodbEvalLimit || 100;
        this.filterExpression = null;
        this.expressionAttributeNames = null;
        this.expressionAttributeValues = null;
        this.dynamoDbReadThroughput = dynamoDbReadThroughput ? Number(dynamoDbReadThroughput) : 25;
        this.limiter = new RateLimiter(this.dynamoDbReadThroughput, 1000);
        this._removeTokens = (tokenCount) => {
            return new Promise((resolve, reject) => {
                this.limiter.removeTokens(tokenCount, () => {
                    resolve();
                });
            });
        }
        this.processWords = true;
        this.updateLeaderboard = true;
        this.playFab = new PlayFabService(playFabSettings);
        this.postSocialService = new PostSocialService(ayrSettings);
        this.profanityValidator = new ProfanityValidator(profanityValidatorConfig);

    }

    // setFilterFunction(filterFunction) {
    //     this.filterFunction = filterFunction;
    // }

    setSourcefilterExpression(filterExpression, expressionAttributeNames, expressionAttributeValues) {
        this.filterExpression = filterExpression;
        this.expressionAttributeNames = expressionAttributeNames;
        this.expressionAttributeValues = expressionAttributeValues;
    }

    run() {
        let ctx = this;
        return new Promise(async (resolve, reject) => {
            try {
                let lastEvalKey, startTime, endTime, collectedCount = 0, totalItemCount = 0, iteration = 1, permitsToConsume = 1;
                let totalStartTime = new Date().getTime();


                // const message = getSnatchNewMessage(profile, lastWord, result.wordPoints);
                // await this.postSocialService.postMessage('THE LIBRARIAN is awakening.');

                if (this.processWords) {
                    do {
                        startTime = new Date().getTime();
                        await ctx._removeTokens(permitsToConsume);

                        let sourceItemResponse = await ctx.dynamoDBDAO.scan(ctx.filterExpression, ctx.expressionAttributeNames, ctx.expressionAttributeValues, lastEvalKey, ctx.dynamodbEvalLimit);
                        totalItemCount += sourceItemResponse.Count;
                        let consumedCapacity = sourceItemResponse.ConsumedCapacity.CapacityUnits;
                        console.log('Consumed capacity ', consumedCapacity);
                        console.log('Received ', sourceItemResponse.Count, ' items at iteration ', iteration, ' and total of ', totalItemCount, ' items received');
                        permitsToConsume = Math.round(consumedCapacity - 1);
                        if (permitsToConsume < 1) {
                            permitsToConsume = 1;
                        }
                        let sourceItems = sourceItemResponse && sourceItemResponse.Items ? sourceItemResponse.Items : [];

                        // TODO: Do work
                        for (const wordInfo of sourceItems) {
                            console.log(wordInfo, null, 2);
                            // Snatch Word
                            const collectResult = await ctx.dynamoDBDAO.collectByLibrarian(wordInfo.word,
                                wordInfo.owner,
                                wordInfo.wordPoints + wordInfo.points,
                                'SYSTEM::0',
                                'THE LIBRARIAN');

                            console.log('collectResult:', JSON.stringify(collectResult, null, 2));

                            if (collectResult.status === 'SUCCESS') {
                                collectedCount++;
                                // Send tweet
                                // const lastWord = 'ass';
                                // const wordPoints = 10;
                                if (!this.profanityValidator.isProfanity(wordInfo.word)) {
                                    const message = getCollectMessage(wordInfo.word, wordInfo.wordPoints);
                                    await this.postSocialService.postMessage(message);
                                }
                            }

                            // console.log(`${userInfo.playFabId}: ${userInfo.userId}`);
                            // let result = await this.playFab.setUserData(userInfo.playFabId, { dbUserId: userInfo.userId});
                            // console.log(result, null, 2);
                        }

                        // console.log(sourceItems, null, 2);

                        if (sourceItemResponse && sourceItemResponse.LastEvaluatedKey) {
                            lastEvalKey = sourceItemResponse.LastEvaluatedKey;
                        } else {
                            lastEvalKey = null;
                        }

                        endTime = new Date().getTime();
                        console.log('Loop completion time : ', endTime - startTime, ' ms');
                        iteration++;
                        // lastEvalKey = iteration <= 1000;
                    } while (lastEvalKey);
                    console.log('Collection completed');
                }
                console.log(`Total collected words: ${collectedCount}`);
                if (this.updateLeaderboard) {
                    // TODO: Update stats for all players on this week's leaderboard
                    const size = 1000;
                    let counter = 0;
                    this.limiter = new RateLimiter(20, 1000);

                    // Set top 100 players on leader
                    const leaderboardResult = await this.playFab.getFullLeaderboard();

                    do {
                        await ctx._removeTokens(1);

                        let leaderboardPlayer = leaderboardResult[counter];


                        const dbUserId = await this.playFab.getUserData(leaderboardPlayer.PlayFabId, 'dbUserId');
                        const userInfo = await ctx.dynamoDBDAO.getUser(dbUserId);
                        // console.debug(JSON.stringify(userInfo, null, 2));

                        if (userInfo.status === 'SUCCESS') {
                            console.log(`${new Date()} --> ${leaderboardPlayer.PlayFabId} ${leaderboardPlayer.DisplayName} ${userInfo.totalPoints}`);
                            await this.playFab.setScore(leaderboardPlayer.PlayFabId, userInfo.totalPoints);
                        } else {
                            console.warn(`WARN: Unable to get user from db: ${dbUserId}`);
                        }

                        counter++
                    } while (counter < leaderboardResult.length)
                }
                const totalEndTime = new Date().getTime();
                console.log('Total time : ', totalEndTime - totalStartTime, ' ms');
                resolve();
            } catch (error) {
                console.error(error);
                reject(error);
            }
        });
    }
}

module.exports = CollectionJob;

function getCollectMessage(word, points) {
    const message = `THE LIBRARIAN collected ${word.toUpperCase()} (${points} pts).\nsnatchword.com\n#SnatchWord`;
    return message;
}