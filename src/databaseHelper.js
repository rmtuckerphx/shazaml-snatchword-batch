'use strict';

const AWS = require('aws-sdk');
const moment = require('moment');
// const log = require('../jovo-ext/log.js');
const docClient = new AWS.DynamoDB.DocumentClient();
// const WordScoreHelper = require('./wordScoreHelper');

// function getUserId(jovo) {
//   const userId = jovo.$app.$db.formatPrimaryKey(jovo.$user.getId(), jovo, false);
//   // AlexaSkill::amzn1.ask.account.AGPB5U...

//   return userId;
// }

// async function getWord(word) {
//   log.trace(`databaseHelper.getWord - word: ${word}`);

//   // console.log('table', process.env.DYNAMODB_USERS_TABLE);
//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `WORD#${word.toUpperCase()}`,
//       SK: `WORD#${word.toUpperCase()}`,
//     },
//     // AttributesToGet: ['word'],
//     // ReturnConsumedCapacity: 'TOTAL',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.get(params).promise();

//     if (Object.keys(data).length === 0 && data.constructor === Object) {
//       return {
//         status: 'NOT_FOUND',
//       };
//     }

//     const now = new Date().toISOString();
//     const currentFindDate = moment(now);
//     const lastFindDay = moment(data.Item.lastFindAt);
//     const pointsAdjust = lastFindDay.diff(currentFindDate, 'days');
//     data.Item.pointsAdjust = pointsAdjust;
//     data.Item.totalPoints = data.Item.points + data.Item.wordPoints;

//     return {
//       status: 'SUCCESS',
//       data: data.Item,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function getUser(userId) {
//   log.trace(`databaseHelper.getUser - userId: ${userId}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `USER#${userId}`,
//       SK: `USER#${userId}`,
//     },
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.get(params).promise();

//     if (Object.keys(data).length === 0 && data.constructor === Object) {
//       return {
//         status: 'NOT_FOUND',
//       };
//     }

//     const totalPoints = data.Item.bonusPoints + data.Item.points;

//     return {
//       status: 'SUCCESS',
//       data: data.Item,
//       totalPoints,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function getPlayer(playerId) {
//   log.trace(`databaseHelper.getPlayer - playerId: ${playerId}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `PLAYER#${playerId}`,
//       SK: `PLAYER#${playerId}`,
//     },
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.get(params).promise();

//     if (Object.keys(data).length === 0 && data.constructor === Object) {
//       return {
//         status: 'NOT_FOUND',
//       };
//     }

//     return {
//       status: 'SUCCESS',
//       data: data.Item,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function getOwnerWords(userId, limit=11) {
//   log.trace(`databaseHelper.getOwnerWords - userId: ${userId}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     IndexName: 'OwnerWordPoints',
//     KeyConditionExpression: '#pk = :u',
//     ExpressionAttributeNames: {
//       '#pk': 'PK1',
//       '#u': 'userId',
//       '#p': 'points',
//       '#b': 'bonusPoints',
//       '#w': 'word',
//       '#o': 'owner',
//       '#t': 'type',
//       '#x': 'expiresAt',
//     },
//     ExpressionAttributeValues: {
//       ':u': `USER#${userId}`,
//     },
//     ScanIndexForward: false,
//     ProjectionExpression: '#t, #u, #p, #b, #w, #o, #x',
//     Limit: limit,
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.query(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function getOwnerExpiredWords(userId, limit=11) {
//   log.trace(`databaseHelper.getOwnerExpiredWords - userId: ${userId}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     IndexName: 'OwnerWordExpiresAt',
//     KeyConditionExpression: '#pk = :u',
//     ExpressionAttributeNames: {
//       '#pk': 'PK1',
//       '#u': 'userId',
//       '#p': 'points',
//       '#b': 'bonusPoints',
//       '#w': 'word',
//       '#o': 'owner',
//       '#t': 'type',
//       '#x': 'expiresAt',
//     },
//     ExpressionAttributeValues: {
//       ':u': `USER#${userId}`,
//     },
//     ScanIndexForward: true,
//     ProjectionExpression: '#t, #u, #p, #b, #w, #o, #x',
//     Limit: limit,
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.query(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function getOwnerSnatchedWords(userId, limit=11) {
//   log.trace(`databaseHelper.getOwnerSnatchedWords - userId: ${userId}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     KeyConditionExpression: '#pk = :u AND begins_with(#sk, :s)',
//     ExpressionAttributeNames: {
//       '#pk': 'PK',
//       '#sk': 'SK',
//       '#w': 'word',
//       '#p': 'newPlayerName',
//       '#t': 'type',
//       '#a': 'action',
//       '#c': 'createAt',
//     },
//     ExpressionAttributeValues: {
//       ':u': `USER#${userId}`,
//       ':s': 'SNATCH#',
//     },
//     ScanIndexForward: true,
//     ProjectionExpression: '#t, #p, #w, #a, #c',
//     Limit: limit,
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.query(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function snatchNewWord(word, userId, playerName) {
//   log.trace(`databaseHelper.snatchNewWord - word: ${word}, userId: ${userId}`);

//   const now = new Date().toISOString();
//   const points = 0;
//   const wordPoints = WordScoreHelper.wordScore(word);
//   const bonusPoints = Number(process.env.FIRST_SNATCH_POINTS || 10);

//   const params = {
//     TransactItems: [{
//       Put: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Item: {
//           PK: `WORD#${word.toUpperCase()}`,
//           SK: `WORD#${word.toUpperCase()}`,
//           type: 'WORD',
//           word,
//           points,
//           wordPoints,
//           createAt: now,
//           lastFindAt: now,
//           expiresAt: moment(now).add(process.env.EXPIRE_DAYS, 'd').toISOString(),
//           owner: userId,
//           firstOwner: userId,
//           player: playerName,
//           PK1: `USER#${userId}`,
//           // GSI2PK: `WORD#${word.toUpperCase()}`,
//         },
//         ReturnValues: 'ALL_OLD',
//         ConditionExpression: 'attribute_not_exists(PK)',
//       },
//     },
//     {
//       Update: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Key: {
//           PK: `USER#${userId}`,
//           SK: `USER#${userId}`,
//         },
//         UpdateExpression: 'ADD #b :b, #p :p',
//         ConditionExpression: 'attribute_exists(PK)',
//         ExpressionAttributeNames: {
//           '#b': 'bonusPoints',
//           '#p': 'points',
//         },
//         ExpressionAttributeValues: {
//           ':b': bonusPoints,
//           ':p': wordPoints + points,
//         },
//         ReturnValues: 'ALL_OLD',
//       },
//     }],
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     // const data = await docClient.transactWrite(params).promise();
//     const data = await executeTransactWrite(params);

//     return {
//       status: 'SUCCESS',
//       data,
//       bonusPoints,
//       wordPoints: points + wordPoints,
//       totalPoints: bonusPoints + points + wordPoints,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }


async function snatchOwnedByOther(word,
  oldOwner,
  totalPoints,
  newUserId,
  newPlayerName,
  ignoreExpiresAt = false) {
  // eslint-disable-next-line max-len
  log.trace(`databaseHelper.snatchOwnedByOther - word: ${word}, oldOwner: ${oldOwner}, totalPoints: ${totalPoints}, newUserId: ${newUserId}, newPlayerName: ${newPlayerName}`);

  const now = new Date().toISOString();
  const max = new Date(253402214400000).toISOString();

  // const snatchAction = ignoreExpiresAt ? 'STOLEN' : 'SNATCHED';
  const snatchAction = 'COLLECTED';

  const params = {
    TransactItems: [
      {
        Update: {
          TableName: process.env.DYNAMODB_USERS_TABLE,
          Key: {
            PK: `WORD#${word.toUpperCase()}`,
            SK: `WORD#${word.toUpperCase()}`,
          },
          UpdateExpression: 'SET #o = :u, #x = :d, #gpk = :k, #p = :p',
          ConditionExpression: 'attribute_exists(PK) AND #o <> :u AND #x <= :x',
          ExpressionAttributeNames: {
            '#x': 'expiresAt',
            '#o': 'owner',
            '#gpk': 'PK1',
            '#p': 'player',
          },
          ExpressionAttributeValues: {
            ':x': ignoreExpiresAt ? max : now,
            ':d': moment(now).add(process.env.EXPIRE_DAYS, 'd').toISOString(),
            ':u': newUserId,
            ':k': `USER#${newUserId}`,
            ':p': newPlayerName,
          },
          ReturnValues: 'ALL_OLD',
        },
      },
      {
        Update: {
          TableName: process.env.DYNAMODB_USERS_TABLE,
          Key: {
            PK: `USER#${newUserId}`,
            SK: `USER#${newUserId}`,
          },
          UpdateExpression: 'ADD #p :p',
          ConditionExpression: 'attribute_exists(PK)',
          ExpressionAttributeNames: {
            '#p': 'points',
          },
          ExpressionAttributeValues: {
            ':p': totalPoints,
          },
          ReturnValues: 'ALL_OLD',
        },
      },
      {
        Update: {
          TableName: process.env.DYNAMODB_USERS_TABLE,
          Key: {
            PK: `USER#${oldOwner}`,
            SK: `USER#${oldOwner}`,
          },
          UpdateExpression: 'ADD #p :p',
          ConditionExpression: 'attribute_exists(PK)',
          ExpressionAttributeNames: {
            '#p': 'points',
          },
          ExpressionAttributeValues: {
            ':p': totalPoints * -1,
          },
          ReturnValues: 'ALL_OLD',
        },
      },
      // {
      //   Put: {
      //     TableName: process.env.DYNAMODB_USERS_TABLE,
      //     Item: {
      //       PK: `USER#${oldOwner}`,
      //       SK: `SNATCH#${word.toUpperCase()}`,
      //       type: 'SNATCH',
      //       action: snatchAction,
      //       word,
      //       createAt: now,
      //       newPlayerName,
      //       ttl: Math.floor(Date.now() / 1000) + 86400, //24 hours in future
      //     },
      //     ReturnValues: 'ALL_OLD',
      //     ConditionExpression: 'attribute_not_exists(PK)',
      //   },
      // }
    ],
  };

  console.log('params:', JSON.stringify(params, null, 2));

  try {
    const data = await executeTransactWrite(params);

    return {
      status: 'SUCCESS',
      data,
      action: snatchAction,
    };
  } catch (error) {
    log.trace(error);
    return {
      status: error.code,
      error,
    };
  }

}


// async function renewWord(word, userId, ignoreRenewAt = false) {
//   log.trace(`databaseHelper.renewWord - word: ${word}, userId: ${userId}`);

//   const now = new Date().toISOString();
//   const renewPoint = moment(now).add(process.env.RENEW_HOURS, 'h').toISOString();
//   const newExpiresAt = moment(now).add(process.env.EXPIRE_DAYS, 'd').toISOString();
//   const max = new Date(253402214400000).toISOString();

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `WORD#${word.toUpperCase()}`,
//       SK: `WORD#${word.toUpperCase()}`,
//     },
//     UpdateExpression: 'SET #x = :d',
//     ConditionExpression: 'attribute_exists(PK) AND #o = :u AND #x <= :r',
//     ExpressionAttributeNames: {
//       '#x': 'expiresAt',
//       '#o': 'owner',
//     },
//     ExpressionAttributeValues: {
//       ':d': newExpiresAt,
//       ':r': ignoreRenewAt ? max : renewPoint,
//       ':u': userId,
//     },
//     ReturnValues: 'UPDATED_NEW',
//     // ReturnConsumedCapacity: 'TOTAL',
//     // ReturnItemCollectionMetrics: 'SIZE',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.update(params).promise();

//     const currentFindDate = moment(now);
//     const lastFindDay = moment(data.Attributes.lastFindAt);
//     const pointsAdjust = lastFindDay.diff(currentFindDate, 'days');
//     data.Attributes.pointsAdjust = pointsAdjust;

//     return {
//       status: 'SUCCESS',
//       data: data.Attributes,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }
// }


// async function addUser(userId, playFabId) {
//   log.trace(`databaseHelper.addUser - userId: ${userId}, playFabId: ${playFabId}`);

//   const createAt = new Date().toISOString();

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Item: {
//       PK: `USER#${userId}`,
//       SK: `USER#${userId}`,
//       type: 'USER',
//       userId,
//       playFabId,
//       points: 0,
//       bonusPoints: 0,
//       createAt,
//       PK1: `USER#${userId}`,
//     },
//     ConditionExpression: 'attribute_not_exists(PK)',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.put(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };

//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function addPlayer(playerId, name, avatarUrl, color, country, countryUrl, platform) {
//   log.trace(`databaseHelper.addPlayer - userId: ${playerId}`);

//   const createAt = new Date().toISOString();

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Item: {
//       PK: `PLAYER#${playerId}`,
//       SK: `PLAYER#${playerId}`,
//       type: 'PLAYER',
//       playerId,
//       name,
//       avatarUrl,
//       color,
//       country,
//       countryUrl,
//       platform,
//       createAt,
//     },
//     ConditionExpression: 'attribute_not_exists(PK)',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.put(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };

//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }

// async function addUserWithPlayerProfile(
//   userId,
//   playerId,
//   playerName,
//   playerAvatarUrl,
//   playerColor,
//   locale,
//   countryCode,
//   countryFlagUrl,
//   platform) {
//   log.trace(`databaseHelper.addUserWithPlayerProfile - userId: ${userId}, playerId: ${playerId}`);

//   const createAt = new Date().toISOString();

//   const params = {
//     TransactItems: [{
//       Put: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Item: {
//           PK: `USER#${userId}`,
//           SK: `USER#${userId}`,
//           type: 'USER',
//           userId,
//           points: 0,
//           bonusPoints: 0,
//           createAt,
//           PK1: `USER#${userId}`,
//           playerId,
//           playerName,
//           playerAvatarUrl,
//           playerColor,
//           locale,
//           countryCode,
//           countryFlagUrl,
//           platform,
//         },
//         ConditionExpression: 'attribute_not_exists(PK)',
//       },
//     },
//     {
//       Put: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Item: {
//           PK: `PLAYER#${playerId}`,
//           SK: `PLAYER#${playerId}`,
//           type: 'PLAYER',
//           playerId,
//           playerName,
//           playerAvatarUrl,
//           playerColor,
//           locale,
//           countryCode,
//           countryFlagUrl,
//           platform,
//           createAt,
//         },
//         ConditionExpression: 'attribute_not_exists(PK)',
//       },
//     }],
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await executeTransactWrite(params);

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }


// async function updateCountry(
//   userId,
//   playerId,
//   countryCode,
//   countryFlagUrl) {
//   log.trace(`databaseHelper.updateCountry - userId: ${userId}, playerId: ${playerId}`);

//   const params = {
//     TransactItems: [{
//       Update: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Key: {
//           PK: `USER#${userId}`,
//           SK: `USER#${userId}`,
//         },
//         UpdateExpression: 'SET #c = :c, #f = :f',
//         ConditionExpression: 'attribute_exists(PK)',
//         ExpressionAttributeNames: {
//           '#c': 'countryCode',
//           '#f': 'countryFlagUrl',
//         },
//         ExpressionAttributeValues: {
//           ':c': countryCode,
//           ':f': countryFlagUrl,
//         },
//         // ReturnValues: 'ALL_OLD',
//       },
//     },
//     {
//       Update: {
//         TableName: process.env.DYNAMODB_USERS_TABLE,
//         Key: {
//           PK: `PLAYER#${playerId}`,
//           SK: `PLAYER#${playerId}`,
//         },
//         UpdateExpression: 'SET #c = :c, #f = :f',
//         ConditionExpression: 'attribute_exists(PK)',
//         ExpressionAttributeNames: {
//           '#c': 'countryCode',
//           '#f': 'countryFlagUrl',
//         },
//         ExpressionAttributeValues: {
//           ':c': countryCode,
//           ':f': countryFlagUrl,
//         },
//         // ReturnValues: 'ALL_OLD',
//       },
//     }],
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await executeTransactWrite(params);

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }


// async function findWord(word, userId) {
//   log.trace(`databaseHelper.findWord - word: ${word}, userId: ${userId}`);

//   // 1. increment points if word exists and you are not the owner
//   const now = new Date().toISOString();

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `WORD#${word.toUpperCase()}`,
//       SK: `WORD#${word.toUpperCase()}`,
//     },
//     UpdateExpression: 'ADD #p :p SET #l = :now',
//     ConditionExpression: 'attribute_exists(PK) AND #o <> :u',
//     ExpressionAttributeNames: {
//       '#p': 'points',
//       '#l': 'lastFindAt',
//       '#o': 'owner',
//     },
//     ExpressionAttributeValues: {
//       ':p': 1,
//       ':now': now,
//       ':u': userId,
//     },
//     ReturnValues: 'ALL_OLD',
//     // ReturnConsumedCapacity: 'TOTAL',
//     // ReturnItemCollectionMetrics: 'SIZE',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.update(params).promise();

//     const currentFindDate = moment(now);
//     const lastFindDay = moment(data.Attributes.lastFindAt);
//     const pointsAdjust = lastFindDay.diff(currentFindDate, 'days');
//     data.Attributes.pointsAdjust = pointsAdjust;
//     data.Attributes.totalPoints = data.Attributes.points + data.Attributes.wordPoints;

//     return {
//       status: 'SUCCESS',
//       data: data.Attributes,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }
// }

// async function addOwnerPoints(userId, bonusPoints = 0, points = 0) {
//   log.trace(`databaseHelper.addOwnerPoints - userId: ${userId}, bonusPoints: ${bonusPoints}, points: ${points}`);

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Key: {
//       PK: `USER#${userId}`,
//       SK: `USER#${userId}`,
//     },
//     UpdateExpression: 'ADD #p :p, #b :b',
//     ConditionExpression: 'attribute_exists(PK)',
//     ExpressionAttributeNames: {
//       '#p': 'points',
//       '#b': 'bonusPoints',
//     },
//     ExpressionAttributeValues: {
//       ':p': points,
//       ':b': bonusPoints,
//     },
//     ReturnValues: 'ALL_NEW',
//     // ReturnConsumedCapacity: 'TOTAL',
//     ReturnItemCollectionMetrics: 'SIZE',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.update(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }
// }


// async function testAddWord(word) {
//   log.trace(`databaseHelper.testAddWord - word: ${word}`);

//   const createAt = new Date().toISOString();
//   const owner = 'user1';
//   const points = 0;

//   const params = {
//     TableName: process.env.DYNAMODB_USERS_TABLE,
//     Item: {
//       PK: `WORD#${word.toUpperCase()}`,
//       SK: `WORD#${word.toUpperCase()}`,
//       type: 'WORD',
//       word,
//       points,
//       createAt,
//       expiresAt: moment(createAt).add(process.env.EXPIRE_DAYS, 'd').toISOString(),
//       owner,
//       PK1: `USER#${owner}`,
//       // GSI2PK: `WORD#${word.toUpperCase()}`,
//     },
//     ReturnValues: 'ALL_OLD',
//   };

//   console.log('params:', JSON.stringify(params, null, 2));

//   try {
//     const data = await docClient.put(params).promise();

//     return {
//       status: 'SUCCESS',
//       data,
//     };
//   } catch (error) {
//     log.trace(error);
//     return {
//       status: error.code,
//       error,
//     };
//   }

// }


module.exports = {
  // addOwnerPoints,
  // addPlayer,
  // addUser,
  // addUserWithPlayerProfile,
  // findWord,
  // getOwnerExpiredWords,
  // getOwnerSnatchedWords,
  // getOwnerWords,
  // getPlayer,
  // getUser,
  // getUserId,
  // getWord,
  // renewWord,
  // snatchNewWord,
  snatchOwnedByOther,
  // updateCountry,
  // testAddWord,
};


function executeTransactWrite(params) {
  const transactionRequest = docClient.transactWrite(params);
  let cancellationReasons;
  transactionRequest.on('extractError', (response) => {
    try {
      cancellationReasons = JSON.parse(response.httpResponse.body.toString()).CancellationReasons;
    } catch (err) {
      // suppress this just in case some types of errors aren't JSON parseable
      console.error('Error extracting cancellation error', err);
    }
  });
  return new Promise((resolve, reject) => {
    transactionRequest.send((err, response) => {
      if (err) {
        console.error('Error performing transactWrite', { cancellationReasons, err });
        return reject(err);
      }
      return resolve(response);
    });
  });
}
