/* eslint-disable max-len */
'use strict';

const SocialPost = require('social-post-api'); //Install "npm i social-post-api"

class PostSocialService {
  constructor(settings) {
    if (process.env.FF_POST_SOCIAL === '1') {
      this.social = new SocialPost(settings.secretKey);
    }
  }

  async postMessage(message) {
    if (process.env.FF_POST_SOCIAL === '1') {
      const post = await this.social.post({
        post: message,
        platforms: ['twitter'],
      }).catch(console.error);

      console.log(JSON.stringify(post, null, 2));
    }
  }

}

module.exports = PostSocialService;
