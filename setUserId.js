'use strict';
require('dotenv').config()
const config = require('./config');

(async () => {
    try {
        let sourceConnectionOptions = {
            region: config.AWS_REGION,
            accessKeyId: config.AWS_ACCESS_KEY_ID,
            secretAccessKey: config.AWS_SECRET_ACCESS_KEY
        };

        const playFabSettings = {
            titleId: config.PLAYFAB_TITLE_ID,
            secretKey: config.PLAYFAB_SECRET_KEY,
        };

        const metadata = {
            filterExpression: '#t = :t',
            expressionAttributeNames: {
                '#t': 'type'
            },
            expressionAttributeValues: {
                ':t': 'USER'
            },
        }

        const SetUserIdJob = require('./src/SetUserIdJob');
        const job = new SetUserIdJob(config.DYNAMODB_TABLE_NAME, sourceConnectionOptions, 100, config.DYNAMODB_READ_THROUGHPUT, playFabSettings);
        job.setSourcefilterExpression(metadata.filterExpression, metadata.expressionAttributeNames, metadata.expressionAttributeValues);

        console.log('Running update...')
        await job.run();
        process.exit(0);
    } catch (error) {
        console.error('SetUserIdJob error', error);
        process.exit(1);
    }
})();