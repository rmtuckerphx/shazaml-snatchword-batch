const { PlayFab, PlayFabClient, PlayFabServer } = require('playfab-sdk');
const util = require('util');

class PlayFabService {
  constructor(settings) {
    this.titleId = settings.titleId;
    this.secretKey = settings.secretKey;

    PlayFabServer.settings.titleId = this.titleId;
    PlayFab.settings.developerSecretKey = this.secretKey;
  }

  async setUserData(playFabId, data) {
    const updateUserDataRequest = {
      PlayFabId: playFabId,
      Data: data,
      Permission: 'Public',
    };
    const updateUserDataPromise = util.promisify(PlayFabServer.UpdateUserData);

    const updateUserDataResponse = await updateUserDataPromise(updateUserDataRequest);
    console.debug(JSON.stringify(updateUserDataResponse, null, 2));
    if (updateUserDataResponse.status !== 'OK') {
      return updateUserDataResponse;
    }

    return {
      status: 'OK',
    };
  }

  async getUserData(playFabId, key) {

    let value;

    const getUserDataRequest = {
      PlayFabId: playFabId,
      Keys: [key],
    };

    const getUserDataPromise = util.promisify(PlayFabServer.GetUserData);

    const result = await getUserDataPromise(getUserDataRequest);
    console.debug(JSON.stringify(result, null, 2));
    if (result.status === 'OK') {
      value = result.data.Data[key].Value;
    }

    return value;
  }

  async getFullLeaderboard() {

    const statName = 'score';
    const pageSize = 100;
    let startAt = 0;
    let fullBoard = [];
    let result;

    do {
      // Top n
      const getLeaderboardRequest = {
        MaxResultsCount: pageSize,
        ProfileConstraints: {
          ShowAvatarUrl: false,
          ShowDisplayName: true,
        },
        StartPosition: startAt,
        StatisticName: statName,
      };

      const getLeaderboardPromise = util.promisify(PlayFabServer.GetLeaderboard);

      result = await getLeaderboardPromise(getLeaderboardRequest);
      console.debug(JSON.stringify(result, null, 2));

      if (result.data.Leaderboard.length > 0) {
        fullBoard = fullBoard.concat(result.data.Leaderboard);
        const last = result.data.Leaderboard[result.data.Leaderboard.length - 1];
        startAt = last.Position + 1;
      }

    } while (result.data.Leaderboard.length > 0)

    return fullBoard;
  }


  async getLeaderboard(count = 100) {

    const statName = 'score';

    // Top n
    const getLeaderboardRequest = {
      MaxResultsCount: count,
      ProfileConstraints: {
        ShowAvatarUrl: false,
        ShowDisplayName: true,
      },
      StartPosition: 0,
      StatisticName: statName,
    };

    const getLeaderboardPromise = util.promisify(PlayFabServer.GetLeaderboard);

    const result = await getLeaderboardPromise(getLeaderboardRequest);
    // console.debug(JSON.stringify(result, null, 2));

    return result.data.Leaderboard;
  }

  async getLeaderboardByPlayer(playFabId) {

    const statName = 'score';

    // Top n
    const getLeaderboardRequest = {
      MaxResultsCount: 1,
      PlayFabId: playFabId,
      ProfileConstraints: {
        ShowAvatarUrl: false,
        ShowDisplayName: true,
      },
      StatisticName: statName,
    };

    const getLeaderboardPromise = util.promisify(PlayFabServer.GetLeaderboardAroundUser);

    const result = await getLeaderboardPromise(getLeaderboardRequest);
    // console.debug(JSON.stringify(result, null, 2));

    return result.data.Leaderboard;
  }

  async setScore(playFabId, value) {
    const updatePlayerStatisticsRequest = {
      PlayFabId: playFabId,
      Statistics: [
        {
          StatisticName: 'score',
          Value: value,
        },
      ],
    };
    const updatePlayerStatisticsPromise = util.promisify(PlayFabServer.UpdatePlayerStatistics);

    const result = await updatePlayerStatisticsPromise(updatePlayerStatisticsRequest);
    console.debug('updatePlayerStatistics:', JSON.stringify(result, null, 2));


    if (result.status !== 'OK') {
      return updateUserDataResponse;
    }

    return {
      status: 'OK',
    };
  }

}

module.exports = PlayFabService;
