'use strict';
const RateLimiter = require('limiter').RateLimiter;
const DynamoDBDAO = require('./DynamoDBDAO');
const PlayFabService = require('./PlayFabService');

class SetUserIdJob {
    constructor(tableName, sourceConnectionOptions, dynamodbEvalLimit, dynamoDbReadThroughput, playFabSettings) {
        this.tableName = tableName;
        // this.filterFunction = () => { return true; };
        this.dynamoDBDAO = new DynamoDBDAO(tableName, sourceConnectionOptions.region, sourceConnectionOptions.accessKeyId, sourceConnectionOptions.secretAccessKey);
        this.dynamodbEvalLimit = dynamodbEvalLimit || 100;
        this.filterExpression = null;
        this.expressionAttributeNames = null;
        this.expressionAttributeValues = null;
        this.dynamoDbReadThroughput = dynamoDbReadThroughput ? Number(dynamoDbReadThroughput) : 25;
        this.limiter = new RateLimiter(this.dynamoDbReadThroughput, 1000);
        this._removeTokens = (tokenCount) => {
            return new Promise((resolve, reject) => {
                this.limiter.removeTokens(tokenCount, () => {
                    resolve();
                });
            });
        }
        this.playFab = new PlayFabService(playFabSettings);

    }

    // setFilterFunction(filterFunction) {
    //     this.filterFunction = filterFunction;
    // }

    setSourcefilterExpression(filterExpression, expressionAttributeNames, expressionAttributeValues) {
        this.filterExpression = filterExpression;
        this.expressionAttributeNames = expressionAttributeNames;
        this.expressionAttributeValues = expressionAttributeValues;
    }

    run() {
        let ctx = this;
        return new Promise(async (resolve, reject) => {
            try {
                let lastEvalKey, startTime, endTime, totalItemCount = 0, iteration = 1, permitsToConsume = 1;
                let totalStartTime = new Date().getTime();
                do {
                    startTime = new Date().getTime();
                    await ctx._removeTokens(permitsToConsume);

                    let sourceItemResponse = await ctx.dynamoDBDAO.scan(ctx.filterExpression, ctx.expressionAttributeNames, ctx.expressionAttributeValues, lastEvalKey, ctx.dynamodbEvalLimit);
                    totalItemCount += sourceItemResponse.Count;
                    let consumedCapacity = sourceItemResponse.ConsumedCapacity.CapacityUnits;
                    console.log('Consumed capacity ', consumedCapacity);
                    console.log('Received ', sourceItemResponse.Count, ' items at iteration ', iteration, ' and total of ', totalItemCount, ' items received');
                    permitsToConsume = Math.round(consumedCapacity - 1);
                    if (permitsToConsume < 1) {
                        permitsToConsume = 1;
                    }
                    let sourceItems = sourceItemResponse && sourceItemResponse.Items ? sourceItemResponse.Items : [];

                    // TODO: Do work
                    for (const userInfo of sourceItems) {
                        if (userInfo.playFabId) {
                        // console.log(userInfo, null, 2);
                        console.log(`${userInfo.playFabId}: ${userInfo.userId}`);
                        let result = await this.playFab.setUserData(userInfo.playFabId, { dbUserId: userInfo.userId});
                        console.log(result, null, 2);
                        }
                    }


                    if (sourceItemResponse && sourceItemResponse.LastEvaluatedKey) {
                        lastEvalKey = sourceItemResponse.LastEvaluatedKey;
                    } else {
                        lastEvalKey = null;
                    }

                    endTime = new Date().getTime();
                    console.log('Loop completion time : ', endTime - startTime, ' ms');
                    iteration++;
                    // lastEvalKey = iteration <= 1000;
                } while (lastEvalKey);
                console.log('SetUserId completed');
                totalEndTime = new Date().getTime();
                console.log('Total time : ', totalEndTime - totalStartTime, ' ms');

                resolve();
            } catch (error) {
                console.error(error);
                reject(error);
            }
        });
    }
}

module.exports = SetUserIdJob;
